import Vue from 'vue'
import Router from 'vue-router'
import MainComponent from '@/components/MainComponent'
import BusinessDetailsComponent from '@/components/BusinessDetailsComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: MainComponent
    },
    {
      path: '/business-details',
      name: 'Details',
      component: BusinessDetailsComponent,
      props: true
    }
  ]
})
