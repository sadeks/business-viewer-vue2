// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import router from './router'
import _ from 'lodash'
import * as VueGoogleMaps from 'vue2-google-maps';
import apiKeys from '../config/ApiKeys';
import GeoLocationComponent from './components/GeoLocationComponent'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: apiKeys.GoogleApiKey,
    libraries: "places" // necessary for places input
  }
});

Vue.component('GeoLocationComponent', GeoLocationComponent);

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
